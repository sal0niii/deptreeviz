deptreeviz – a library to view and edit dependency trees
========================================================

We at [NatS](https://nats-www.informatik.uni-hamburg.de) have a long
history of visualizing dependency trees.  This library is a spin-off
from our dependency parser [jwcdg](https://gitlab.com/nats/jwcdg),
which comes with its own editing and visualization tools.

The trees look like this:

![example tree](https://nats-www.informatik.uni-hamburg.de/pub/User/ArneKoehn/ralston_purina.svg)

The library can also cope with circular annotations:

```
1	circles	circles	NOUN	NOUN	_	1	root	_	_
2	are	are	VERB	VERB	_	3	dobj	_	_
3	very	very	ADJ	ADJ	_	4	dobj	_	_
4	bad	bad	ADJ	ADJ	_	2	dobj	_	_
```

yields this:

![circular annotation](https://nats-www.informatik.uni-hamburg.de/pub/User/ArneKoehn/circular.svg)

Usage as a program
------------------

Simply clone this repository, build it with 
```./gradlew shadowJar```

and run ```java -jar build/libs/deptreeviz-[version]-all.jar``` with
either two arguments (conll in, svg out) or without arguments and pipe
your conll to stdin and get the svg with stdout.


Usage as a library
------------------

deptreeviz is
[published on maven central](https://search.maven.org/#search%7Cga%7C1%7Ca%3A%22deptreeviz%22).
You can simply include it with maven/gradle/sbt/whatever using the
GroupId io.gitlab.nats and the ArtifactId deptreeviz.


src/main/java/io/gitlab/nats/deptreeviz/ConllToSVG.java

shows the basic usage.  More documentation might be on the way,
especially a demo for swing integration and editing trees using a GUI.

Features not well documented:

 - edge highlighting
 - displaying additional node info (PoS etc.)
 - displaying multiple layers of annotation, e.g.syntax and
   referential reliations

Authors and license
-------------------

This Code was mainly written by Sven Zimmer.  Arne Köhn extracted the
library from the jwcdg code.

This library is licensed under the Apache License, Version 2.0.
